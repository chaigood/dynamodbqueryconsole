﻿using Amazon.DynamoDBv2.DataModel;

namespace DynamoDBQueryConsole.Model {
    [DynamoDBTable("Customer")]

    public class Customer { 
    [DynamoDBHashKey("CustomerID")]
    public string customerid { get; set; }

    [DynamoDBProperty("FirstName")]
    public string FirstName { get; set; }

    [DynamoDBProperty("LasName")]
    public string LastName { get; set; }
    }

}
