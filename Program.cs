﻿using Amazon;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;
using System.Threading.Tasks;
using System.Linq;
using System;
using DynamoDBQueryConsole.Model;


namespace DynamoDBQueryConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            var client = new AmazonDynamoDBClient("AKIAVIWHBLSRSPCR7KYW", "omhJQJKLyd9/eEzvuNulTSSf8GIQuOV+Oth1B4ua", RegionEndpoint.USEast1);
            DynamoDBContext context = new DynamoDBContext(client);

            GetCustomerAsync(context, "A11794DE-586C-4B16-B84F-5C5C2981BDFE").Wait();
        }

        private static async Task GetCustomerAsync(DynamoDBContext context, string customerId)
        {
            var customer = await context.QueryAsync<Customer>(customerId, new DynamoDBOperationConfig { OverrideTableName = "Customer", IndexName = "CustomerID-index" }).GetRemainingAsync();
            var c = customer.FirstOrDefault();
            Console.WriteLine($"{c.FirstName} {c.LastName}");
            //Console.WriteLine("Id:{0}\t Firstname:{1}\t Lastname:{2}", c.FirstName, c.LastName);
            Console.WriteLine("Press any key to continue...");
            Console.ReadLine();
        }

    }
}